## Ejercicio - Método Show Even

Define el método `show_even` que recibe un arreglo como argumento y le suma 2 a cada elemento del arreglo y solamente regresa los números pares. El resultado de la prueba `driver code` debe ser true.

Se entregarán dos versiones de este programa, la primera versión con estructura iterativa `each` y la otra solamente con `métodos enumerables`.

```ruby
#show_even method


#driver code
p show_even([4, 6, 8, 10, 23, 45, 56, 2]) == [6, 8, 10, 12, 58, 4] 
p show_even([12, 34, 21, 1, 2, 7, 89]) == [14, 36, 4]
``` 
